package com.idaltec.msa.products;

import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;

import feign.RequestInterceptor;

public class FeignClientOAUth2Configuration {

    
    @Bean
    public RequestInterceptor oauth2FeignRequestInterceptor(OAuth2ClientContext oauth2ClientContext) {
       System.out.println("*******************************************************");
        return new OAuth2FeignRequestInterceptor(oauth2ClientContext, resource());
    }
     
    
    @Bean
    protected OAuth2ProtectedResourceDetails resource() {
       AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
       return resource;
    }
}

