package com.idaltec.msa.products;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@FeignClient(value="Users", url="${url.users}", configuration=FeignClientOAUth2Configuration.class )
public interface UserServiceClient {
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String getUser();
}
 